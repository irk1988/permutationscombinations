/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Visiting permutations/combinations of distinct elements
 */

#include<iostream>
#include<ctime>
#include<cmath>

using namespace std;

int n; // input array length
int p; // variable to store choose 'k' for the sake of printing
int h; // variable to store number of permutations/combinations
int v; // variable for storing verbose option

/*
Visit function - invoked for each permutations/combinations
Function displays the permutation/combination if verbose parameter is greater than 0
*/
int visit( int A[], int verbose, int p )
{
    if( verbose > 0 ) {
        for( int i=1; i<=p; i++ )
            cout<<A[i]<<" ";
        cout<<endl;
    }
    h++;
    return 0;
}

/*
Function to generate combinations for a set of input. (Uses recursion calls)
Pre condition: Some location of A has already been taken by i+1 to n elements
Post condition: Selects k objects from i to i and assigns to the combination
*/
int comb( int A[], int i, int k, int verbose )
{
    if( k == 0 )
        visit(A, verbose, p);
    else if( i < k ) return 0;
    else {
        A[k] = i;
        comb( A, i-1, k-1, verbose);
        A[k] = 0;
        comb( A, i-1, k, verbose);
    }
}

/*
Function to generate the permutations of A - Does so in lexicographic order
*/
int perm ( int A[], int i, int k, int verbose )
{
    int j = n-1;
    int l = n;
    if( p > 0 && p != n-1  )
        visit(A, v, p);

    do {
        if( j == 0 ) break;
        if( j <= p)
            visit(A, v, p);
        l = n-1;
        j = n-2;
        while( (A[j] >= A[j+1]) && j>0) j--;
        while( (A[j] >= A[l]) && l>0) l--;

        int temp;
        temp = A[j];
        A[j] = A[l];
        A[l] = temp;

        int k=j+1;
        l = n-1;

        while(k<l && l>0) {
            temp=A[k];
            A[k] = A[l];
            A[l] = temp;

            k++;
            l--;
        }
    } while(1);
}

int main()
{

    //variable to compute runtime
    clock_t start, end;
    int dur = 0;

    start = clock();

    n=0;
    h=0; //Global variable - initialization
    int k = 0;

    cin>>n>>k>>v;
    p = k;

    //check if it is combination/permutation
    if( v == 1 || v == 3 ) {
        if( k != 0 ) {
            int * A = new int(n);
            for(int i=0; i<n; i++)
                A[i] = 0;
            comb(A, n, k, v);
        }
    } else if( v == 0 || v == 2 ) {
        n++;
        int * A = new int(n); //extra memory for the sentinel to be placed first
        for(int i=1; i<n; i++)
            A[i] = i;
        A[0] = 0;
        perm(A, n, k, v);
    }

    end = clock();

    dur = (int) round((double)(end - start)/CLOCKS_PER_SEC*1000);
    cout<<h<<" "<<dur<<endl;
    return 0;
}
