/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Visiting all permutations with duplicates in lexocographic order
 */

#include<iostream>
#include<ctime>
#include<cmath>
#include<algorithm>

using namespace std;

int h; //Global variable to maintain the count of permutations
int n; //Global variable for number of elements in the input array
int v; //Global variable for verbose option

/*
Visit function that will be invoked for all the permutations for the input
Displays all the elements in the permutation based of parameter verbose
*/
int visit( int A[], int n, int verbose)
{
    if( verbose > 0 ) {
        for( int i=1; i<n; i++ )
            cout<<A[i]<<" ";
        cout<<endl;
    }
    h++;
    return 0;
}


int main()
{

    clock_t start, end;
    int dur = 0;

    start = clock();

    h = 0; //Inital permutation count
    cin>>n>>v;

    n++;
    int * A = new int(n); //extra memory for the sentinel to be placed first

    for(int i=1; i<n; i++)
        cin>>A[i];

    sort(A+1, A+n);
    A[0] = A[1]-1; //Sort the array and place the smallest element - 1 as the sentinel

    int j = n-1;
    int l = n;

    do {
        if( j == 0 ) break;
        visit(A, n, v);
        l = n-1;
        j = n-2;
        while( (A[j] >= A[j+1]) && j>0) j--;
        while( (A[j] >= A[l]) && l>0) l--;

        int temp;
        temp = A[j];
        A[j] = A[l];
        A[l] = temp;

        int k=j+1;
        l = n-1;

        while(k<l && l>0) {
            temp=A[k];
            A[k] = A[l];
            A[l] = temp;

            k++;
            l--;
        }

    } while(1);

    end = clock();

    dur = (int) round((double)(end - start)/CLOCKS_PER_SEC*1000);
    cout<<h<<" "<<dur<<endl;

    return 0;
}
